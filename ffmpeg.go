package main

import (
	"bytes"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

func execCommand(cmd exec.Cmd) {
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Printf("Error running the below:\n%s\n", cmd.String())
		log.Fatal(err)
	}
}

func createMp3(activationBytes string, filename string) {
	filenameWithoutExtention := filename[:len(filename)-4]
	newFilename := filenameWithoutExtention + ".mp3"
	var cmd *exec.Cmd = exec.Command(
		"ffmpeg", "-y",
		"-activation_bytes", activationBytes,
		"-i", filename,
		"-map_metadata", "0", "-id3v2_version", "3", "-codec:a", "libmp3lame",
		"-vn", newFilename,
	)

	if _, err := os.Stat(newFilename); err == nil {
		log.Printf("Skipping MP3 - %s\n", newFilename)

	} else if os.IsNotExist(err) {
		log.Printf("Creating MP3 - %s\n", newFilename)
		execCommand(*cmd)
	}
}

func createM4b(activationBytes string, filename string) {
	filenameWithoutExtention := filename[:len(filename)-4]
	tmpFilename := filenameWithoutExtention + ".m4a"
	newFilename := filenameWithoutExtention + ".m4b"
	var cmd = exec.Command(
		"ffmpeg", "-y",
		"-activation_bytes", activationBytes,
		"-i", filename,
		"-map_metadata", "0", "-id3v2_version", "3", "-codec:a", "copy",
		"-vn", tmpFilename,
	)

	if _, err := os.Stat(newFilename); err == nil {
		log.Printf("Skipping M4B - %s\n", newFilename)
	} else if os.IsNotExist(err) {
		log.Printf("Creating M4B - %s\n", newFilename)
		execCommand(*cmd)
		err = os.Rename(tmpFilename, newFilename)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func iterate(path string, activationBytes string, mp3 bool, m4b bool) {
	var err = filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatal(err)
		}

		name := info.Name()

		if len(name) > 5 {
			extention := name[len(name)-4:]
			if extention == ".aax" || extention == ".mp4" {
				log.Printf("Processing file - %s\n", name)
				if mp3 {
					createMp3(activationBytes, path)
				}
				if m4b {
					createM4b(activationBytes, path)
				}
			}
		}

		return nil
	})
	if err != nil {
		log.Fatal(err)
	}
}
