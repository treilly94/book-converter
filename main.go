package main

import (
	"flag"
	"log"
	"os"
	"strconv"
)

var (
	activationBytes = flag.String("a", os.Getenv("ACTIVATION_BYTES"), "Activation Bytes to convert file")
	mp3             = flag.Bool("mp3", true, "Whether to create a mp3 file")
	m4b             = flag.Bool("m4b", false, "Whether to create a m4b file")
	pwd, _          = os.Getwd()
)

func main() {
	flag.Parse()
	if *activationBytes == "" {
		log.Fatal("Error: Activation bytes can not be blank")
	}

	log.Printf("Config - activation bytes: %s\n", *activationBytes)
	log.Printf("Config - mp3: %s\n", strconv.FormatBool(*mp3))
	log.Printf("Config - m4b: %s\n", strconv.FormatBool(*m4b))
	log.Printf("Config - working directory: %s\n", pwd)

	iterate(pwd, *activationBytes, *mp3, *m4b)
}
