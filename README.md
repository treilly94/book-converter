# Book Converter

This app converts audiobooks to other formats.

It will convert all audiobook files recursively in the directory its run in.

## Config

### Activation Bytes
Activation bytes can be passed to the app by using the `-a` flag on the commandline or by setting the `ACTIVATION_BYTES` environment variable.

## Examples
With commandline arguments:
```
audiobooks -a qwerty
```

With environment variables:
```
export ACTIVATION_BYTES=qwerty
audiobooks
```

To get help:
```
audiobooks -h
```

## Development Lifecycle

### Linting
Linting relies on [golangci-lint](https://golangci-lint.run/) and can be run with the command:
```
make lint
```

### Run
The code can be run for development purposes using the below command:
```
make run
```

### Build
The below command builds binaries for multiple linux architectures and puts them in the `bin/` directory:
```
make build
```

### Upload
The below command uploads all binaries to [Gitlab](https://gitlab.com/treilly94/book-converter/-/packages):
```
make upload
```

### Install
The below command builds the code and installs it in your gopath:
```
make install
```
