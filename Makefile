NAME=audiobooks
VERSION?=test
BINARY_DIRECTORY=bin/
BINARY_PATH=${BINARY_DIRECTORY}${NAME}

GIT_PROJECT_ID=31375398
DOCKER_REGISTRY=registry.gitlab.com/treilly94/book-converter/
UPLOAD_TOKEN?=

OS?=linux
ARCH?=amd64

.PHONY:lint
lint:
	golangci-lint run

.PHONY: run
run:
	go run .

.PHONY: build
build:
	GOOS=${OS} GOARCH=${ARCH} go build -o ${BINARY_PATH} .

.PHONY: upload
upload:
	curl --header "JOB-TOKEN: ${UPLOAD_TOKEN}" --upload-file ${BINARY_PATH} \
	"https://gitlab.com/api/v4/projects/${GIT_PROJECT_ID}/packages/generic/${NAME}/${VERSION}-${OS}-${ARCH}/${NAME}"

.PHONY: install
install:
	go install
